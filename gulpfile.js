
'use strict';

///////////////////////////////////////////////////
////
////   FRONT END AUTOMATION
////
//////////////////////////////////////////////////


var gulp = require('gulp');
var watch=require('gulp-watch');
var browsersync = require('browser-sync').create();
var del=require('del');
var removeunusedcss=require('purify-css');
/////Plugin for requiring all the plugins automatically
var $ = require('gulp-load-plugins')({
	rename:{
		'gulp-sass':'sass',
		'gulp-plumber':'check',
		'gulp-clean-css':'cssmini',
		'gulp-concat':'concat',
		'gulp-autoprefixer':'cssprefix',
		'gulp-jshint':'checkjs',
		'gulp-uglify':'jsmini',

	}
});



///////////////////////////////////////
// Styles Task
///////////////////////////////////////
gulp.task('styles',function(){
	return gulp.src(['app/bower_components/bootstrap/dist/css/bootstrap.min.css','app/scss/style.scss'])
	.pipe($.check())
	.pipe($.sass())
	.pipe($.cssprefix({browsers:['last 2 versions']}))
	.pipe($.cssmini())
	.pipe($.concat('style.css'))
	.pipe(gulp.dest('app/css'))
	.pipe(browsersync.stream());

});

///////////////////////////////////////
// JS Task
///////////////////////////////////////
gulp.task('scripts',function(){
	return gulp.src(['app/bower_components/jquery/dist/jquery.js','app/bower_components/tether/dist/js/tether.js','app/bower_components/bootstrap/dist/js/bootstrap.js','app/js/**/*.js','!app/js/*.min.js'])
	.pipe($.jsmini())
	.pipe(gulp.dest('app/js'))
	.pipe(browsersync.stream());
});

///////////////////////////////////////
// html Task
///////////////////////////////////////
gulp.task('html',function(){
	return gulp.src('app/**/*.html')
	.pipe(browsersync.stream());
});

///////////////////////////////////////
// Watch Task
///////////////////////////////////////
gulp.task('watch',function(){
	 gulp.watch('app/js/main.js',['scripts']);
	 gulp.watch('app/scss/style.scss',['styles']);
	 gulp.watch('app/**/*.html',['html']);
});


///////////////////////////////////////
// Browser Sync
///////////////////////////////////////
gulp.task('browser-sync',function(){
	browsersync.init({
		server:{
			baseDir:"./app/"
		}
	});
});

gulp.task('build:serve',function(){
	browsersync.init({
		server:{
			baseDir:"./build/"
		}
	});
});

//////////////////////////////////
// FINAL PRODUCT BUILD
/////////////////////////////////




gulp.task('build:copy',function(){
	return gulp.src('app/**/*/')
	.pipe(gulp.dest('build/'));
})

gulp.task('build:removeextra',['build:copy'],function(){
	var content=['build/js/main.js','build/**/*.html'];
	var css=['build/css/style.css'];
	var options={
		output:'build/css/style.css',
		minify: true,
		rejected: true
	};
	return removeunusedcss(content,css,options);
})

gulp.task('build:remove',['build:removeextra'],function(cb){
	del([
		'build/scss/',
		'build/bower_components/',
		'build/js/!(*.min.js)'
		],cb);
});

gulp.task('build',['build:copy','build:remove']);

///////////////////////////////////////
// Default Task
///////////////////////////////////////
gulp.task('default',['styles','scripts','browser-sync','watch']);


